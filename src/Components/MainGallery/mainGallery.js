import styles from "./mainGallery.module.css";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { postService } from "../../services/post.services";
import { authService } from "../../services/auth.service";

const MainGallery = () => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    postService.getPosts().then((posts) => setPosts(posts));
  }, []);

  

  return (
    <div className={styles.Gallery_Container}>
      <div className={styles.Gallery_Grid_Container}>
        {posts.map((post) => {
          return (
            <Link to="/post" className={styles.Gallery_Grid_Item} key={post.id}>
              <img src={post.profile_image} alt="gridimage1"></img>
              <div className={styles.Grid_Item_Info}>
                {/* STARTS HERE  */}
                <span>{post.created_at.toDate().toLocaleDateString()}</span>
                <span className={styles.Line}></span>
                <span className={styles.Grid_View}>
                  <svg
                    width="14"
                    height="12"
                    viewBox="0 0 14 12"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M0.638672 6.00065C1.48801 3.29598 4.01534 1.33398 7 1.33398C9.98534 1.33398 12.512 3.29598 13.3613 6.00065C12.512 8.70532 9.98534 10.6673 7 10.6673C4.01534 10.6673 1.48801 8.70532 0.638672 6.00065Z"
                      stroke="#AEBEC9"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                  <p>{post.view_count}</p>
                </span>
              </div>
              <p>{post.title}</p>
            </Link>
          );
        })}
      </div>
    </div>
  );
};

export default MainGallery;