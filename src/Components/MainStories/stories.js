import styles from './stories.module.css';
import storyImage1 from '../../assets/Ellipse 158.png'
import storyImage2 from '../../assets/story2.png'
import storyImage3 from '../../assets/story3.png'

const MainStories = () => {
    return (
    <div className={styles.StoriesContainer}>
        <h3>История последних новостей</h3>
        <div className={styles.Stories_Flex_Container}>
            <a className={styles.Stories_Element} href='/'>
                <img src={storyImage1} alt='story1'></img>
            </a>
            <a className={styles.Stories_Element} href='/'>
                <img src={storyImage2} alt='story2'></img>
            </a>
            <a className={styles.Stories_Element} href='/'>
                <img src={storyImage3} alt='story3'></img>
            </a>
            <a className={styles.Stories_Element} href='/'>
                <img src={storyImage1} alt='story4'></img>
            </a>
            <a className={styles.Stories_Element} href='/'>
            <img src={storyImage3} alt='story5'></img>
            </a>
        </div>
    </div>
)
}

export default MainStories