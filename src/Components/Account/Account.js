import React from 'react'
import styles from './account.module.css'
import Header from '../Header/Header'
import Footer from '../Footer/footer'
import Avatar from '../../assets/avatar.png'
import Publication from '../../assets/publication.png'

function Account() {
return (
    <div className={styles.TopContainer}>
        <div className={styles.Container}>
        <Header/>
        <div className={styles.InfoWrapper}>
            <div><img src={Avatar} alt='avatar'></img></div>
            <div className={styles.InfoContent}>
                <h2>Дилором Алиева</h2>
                <table cellspacing="0" cellpadding="0">
                <tr>
                    <th>Карьера</th>
                    <td>Писатель</td>
                </tr>
                <tr>
                    <th>Дата рождения</th>
                    <td>2 ноября, 1974  ( 46 лет)</td>
                </tr>
                <tr>
                    <th>Место рождения</th>
                    <td>Черняховск, СССР (Россия)</td>
                </tr>
                </table>
            </div>
        </div>
        <h2>ПУБЛИКАЦИИ</h2>
        <div className={styles.PublicationsWraper}>
            <div className={styles.PublicationItem}>
                <img src={Publication} alt='publication'></img>
                <div className={styles.PublicationInfo}>
                    <h2>По инициативе Узбекистана создана Группа друзей по правам молодежи</h2>
                    <div className={styles.Grid_Item_Info}>
                        <span>18:26  11.01.2021</span>
                        <span className={styles.Line}></span>
                        <span className={styles.Grid_View}>
                            <svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.638672 6.00065C1.48801 3.29598 4.01534 1.33398 7 1.33398C9.98534 1.33398 12.512 3.29598 13.3613 6.00065C12.512 8.70532 9.98534 10.6673 7 10.6673C4.01534 10.6673 1.48801 8.70532 0.638672 6.00065Z" stroke="#AEBEC9" strokeLinecap="round" strokeLinejoin="round"/></svg>
                            <p>365</p>
                        </span> 
                        <span className={styles.Humanright}>
                            Права человека
                        </span>
                    </div>
                    <p>Посланник Генерального секретаря ООН по делам молодежи Джаятма Викраманаяке приняла участие в презентации созданной по инициативе Узбекистана Группе друзей по правам молодежи. В рамках этого международного проекта планируется продвижение прав молодых жителей планеты и расшире...</p>
                </div>
            </div>
            <div className={styles.PublicationItem}>
                <img src={Publication} alt='publication'></img>
                <div className={styles.PublicationInfo}>
                    <h2>По инициативе Узбекистана создана Группа друзей по правам молодежи</h2>
                    <div className={styles.Grid_Item_Info}>
                        <span>18:26  11.01.2021</span>
                        <span className={styles.Line}></span>
                        <span className={styles.Grid_View}>
                            <svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.638672 6.00065C1.48801 3.29598 4.01534 1.33398 7 1.33398C9.98534 1.33398 12.512 3.29598 13.3613 6.00065C12.512 8.70532 9.98534 10.6673 7 10.6673C4.01534 10.6673 1.48801 8.70532 0.638672 6.00065Z" stroke="#AEBEC9" strokeLinecap="round" strokeLinejoin="round"/></svg>
                            <p>365</p>
                        </span> 
                        <span className={styles.Humanright}>
                            Права человека
                        </span>
                    </div>
                    <p>Посланник Генерального секретаря ООН по делам молодежи Джаятма Викраманаяке приняла участие в презентации созданной по инициативе Узбекистана Группе друзей по правам молодежи. В рамках этого международного проекта планируется продвижение прав молодых жителей планеты и расшире...</p>
                </div>
            </div>
            <div className={styles.PublicationItem}>
                <img src={Publication} alt='publication'></img>
                <div className={styles.PublicationInfo}>
                    <h2>По инициативе Узбекистана создана Группа друзей по правам молодежи</h2>
                    <div className={styles.Grid_Item_Info}>
                        <span>18:26  11.01.2021</span>
                        <span className={styles.Line}></span>
                        <span className={styles.Grid_View}>
                            <svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.638672 6.00065C1.48801 3.29598 4.01534 1.33398 7 1.33398C9.98534 1.33398 12.512 3.29598 13.3613 6.00065C12.512 8.70532 9.98534 10.6673 7 10.6673C4.01534 10.6673 1.48801 8.70532 0.638672 6.00065Z" stroke="#AEBEC9" strokeLinecap="round" strokeLinejoin="round"/></svg>
                            <p>365</p>
                        </span> 
                        <span className={styles.Humanright}>
                            Права человека
                        </span>
                    </div>
                    <p>Посланник Генерального секретаря ООН по делам молодежи Джаятма Викраманаяке приняла участие в презентации созданной по инициативе Узбекистана Группе друзей по правам молодежи. В рамках этого международного проекта планируется продвижение прав молодых жителей планеты и расшире...</p>
                </div>
            </div>
        </div>
        </div>
        <Footer/>
    </div>
    )
}

export default Account