import styles from './header.module.css'
import Logo from '../../assets/NavbarLogo.png'

function Header( { handleLoginClick }) {

        const handleClick = () => {
            handleLoginClick();
        };

    return (
    <div className={styles.Wrapper}>
        <div className={styles.HeaderContainer}>
            <a href='/'><img src={Logo} alt="Logo"></img></a>
            <nav className={styles.Menu_Container}>
                <ul>
                    <li><a href='/'>Все потоки</a></li>
                    <li><a href='/'>Разработка</a></li>
                    <li><a href='/'>Администрирование</a></li>
                    <li><a href='/'>Дизайн</a></li>
                    <li><a href='/'>Менеджмент</a></li>
                    <li><a href='/'>Маркетинг</a></li>
                    <li><a href='/'>Научпоп</a></li>
                </ul>
            </nav>
            <button className={styles.Login_Button} onClick={handleClick} >Войти</button>
        </div>
    </div>
    )
}

export default Header