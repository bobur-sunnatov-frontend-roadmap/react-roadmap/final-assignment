import styles from './main.module.css';
import MainNavMenu from '../MainNavMenu/MainNavMenu'
import MainStories from '../MainStories/stories'
import MainGallery from '../MainGallery/mainGallery';
import Pagination from '../MainPagination/pagination';
import MainNavbar from '../MainNavbar/MainNavbar';
import Login from '../Login/Login';
import SignUp from '../Signup/SignUp';
import { useState } from 'react';
import Footer from '../Footer/footer';



function Main () {
    
    const [isShowLogin, setIsShowLogin] = useState(true);

    const handleLoginClick = () => {
    setIsShowLogin((isShowLogin) => !isShowLogin);
    };
    const [isShowSignup, setIsShowSignup] = useState(true);

    const handleSignupClick = () => {
    setIsShowSignup((isShowSignup) => !isShowSignup);
    };
    return(<>
    <div className={styles.MainContainer}>
        <MainNavbar handleLoginClick={handleLoginClick} />
        <MainNavMenu/>
        <MainStories/>
        <MainGallery/>
        <Pagination/>
        <Login isShowLogin={isShowLogin} handleSignupClick={handleSignupClick}/>
        <SignUp isShowLogin={isShowSignup} />
    </div>
    <Footer/>
    </>
    )
}

export default Main;