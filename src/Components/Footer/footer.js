import styles from './footer.module.css'
import logo from '../../assets/FooterLogo.png'

function Footer() {
    return (
    <footer className={styles.FooterContainer}>
        <div className={styles.FooterFlexContainer}>
            <div className={styles.Logo_About}>
                <img src={logo} alt='logo'></img>
                <p>Помощник в публикации статей, журналов. Список популярных международных конференций. Всё для студентов и преподавателей.</p>
            </div>
            <div className={styles.FooterMenuFlex}>
                <div className={`${styles.MenuColumn} ${styles.ResourcesWrapper}`}>
                    <h4>Ресурсы</h4>
                    <p>Статьи</p>
                    <p>Журналы</p>
                    <p>Газеты</p>
                    <p>Диплом</p>
                </div>
                <div className={`${styles.MenuColumn} ${styles.AboutWrapper}`}>
                    <h4>О нас</h4>
                    <p>Контакты</p>
                    <p>Помощь</p>
                    <p>Заявки</p>
                    <p>Политика</p>
                </div>
                <div className={`${styles.MenuColumn} ${styles.HelpWrapper}`}>
                    <h4>Помощь</h4>
                    <p>Часто задаваемые вопросы</p>
                </div>
            </div>
        </div>
        <div className={styles.CopyRight}>
            <p>Copyright © 2020. LogoIpsum. All rights reserved.</p>
        </div>
    </footer>
    )
}

export default Footer