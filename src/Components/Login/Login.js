import React, { useState, useContext } from 'react'
import styles from './login.module.css'
import { authService } from "../../services/auth.service";
import { Navigate } from 'react-router-dom';
import { Link } from 'react-router-dom';


const Login = ({ isShowLogin, handleSignupClick }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = (e) => {
      e.preventDefault();
      authService.login({
      email: email,
      password: password,  
    });
  }
  const handleClick = () => {
    handleSignupClick();
  };
//   const { currentUser } = useContext(authService)

//   if (currentUser) {
//     return  (<Navigate replace to='/'></Navigate>)
//   } else
return (
    <div className={ `${isShowLogin ? styles.active : ""} ${styles.Wrapper}`}>
        <div className={styles.FormContainer}>
                <form  onSubmit={handleSubmit}>
                    <h2 className={styles.SignIn}>Вход на udevs news</h2>
                    <div className={styles.LoginFlex}>
                        <input type="email" name="username" placeholder='Email' className={styles.LoginInput} onChange={(e) => setEmail(e.target.value)}/>
                        <input type="password" name="password" placeholder='Пароль' className={styles.LoginInput} onChange={(e) => setPassword(e.target.value)}/>
                    </div>
                    <input type="submit" value="Войти" className={styles.LoginButton} />
                    <button className={styles.SignupButton} onClick={handleClick}>Do not have an account? Sign up</button>
                </form>
            
        </div>
    </div>
)
}
export default Login;
