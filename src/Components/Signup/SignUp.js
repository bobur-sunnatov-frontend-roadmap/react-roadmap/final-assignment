import React, { useState } from 'react'
import styles from '../Login/login.module.css'
import { authService } from "../../services/auth.service";
import { useNavigate, Navigate } from "react-router-dom";
import { Link } from "react-router-dom";

function SignUp({ isShowLogin }) {  

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = (e) => {
      e.preventDefault();
      authService.signUp({
      email: email,
      name: name,
      password: password,  
    })
    ;
  }
  return (
    <div>
      <div className={ `${isShowLogin ? styles.active : ""} ${styles.Wrapper}`}>
      <div className={styles.FormContainer}>
            <form id='signup-form' onSubmit={handleSubmit}>
                <h2 className={styles.SignIn}>Register</h2>
                <div className={styles.LoginFlex}>
                    <input type="text" name="name" placeholder='Введите имя' className={styles.LoginInput} id='signup-name' required onChange={(e) => setName(e.target.value)} value={name}/>
                    <input type="email" name="email" placeholder='Email' className={styles.LoginInput} id='signup-email' required onChange={(e) => setEmail(e.target.value)} value={email}/>
                    <input type="password" name="password" placeholder='Придумайте пароль' className={styles.LoginInput} id='signup-password' required onChange={(e) => setPassword(e.target.value)} value={password}/>
                </div>
                <input type="submit" value="Войти" className={styles.LoginButton} />
               
            </form>
        
    </div>
</div></div>
  )
  } 

export default SignUp