import styles from './mainNavMenu.module.css';

const MainNavMenu = () => {
    return (
        <nav className={styles.Menu_Container}>
            <ul>
                <li className={styles.MainPageButton}><a href='/'>Все потоки</a></li>
                <li><a href='/'>Разработка</a></li>
                <li><a href='/'>Администрирование</a></li>
                <li><a href='/'>Дизайн</a></li>
                <li><a href='/'>Менеджмент</a></li>
                <li><a href='/'>Маркетинг</a></li>
                <li><a href='/'>Научпоп</a></li>
            </ul>
        </nav>
    )
}
export default MainNavMenu