import {
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
  } from "firebase/auth";
  import {
    addDoc,
    collection,
    doc,
    getDocs,
    query,
    setDoc,
    where,
  } from "firebase/firestore";
  import { auth, db } from "../firebase";
  
  export const authService = {
    async signUp(
      body = {
        email: "",
        password: "",
        name: "",
        // avatar: "",
      }
    ) {
      try {
        const { email, password, name } = body;
        const userCred = await createUserWithEmailAndPassword(
          auth,
          email,
          password
        );
        console.log(userCred);
        await setDoc(doc(db, "users", userCred.user.uid), {
          email,
          name,
          // avatar,
        });
      } catch (error) {
        return Promise.reject("User exists");
      }
    },


  
    async login(email, password) {
      try {
        const user = await signInWithEmailAndPassword(auth, email, password);
        return {
          id: user.user.uid,
        };
      } catch (error) {
        return Promise.reject("Incorrect email or password");
      }
    },
  };