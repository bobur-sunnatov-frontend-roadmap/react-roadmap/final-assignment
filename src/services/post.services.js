import { collection, getDocs } from "firebase/firestore";
import { db } from "../firebase";

export const postService = {
  getPosts: async () => {
    let posts = [];
    try {
      const postSnap = await getDocs(collection(db, "posts"));
      postSnap.forEach((doc) => {
        posts.push({
          id: doc.id,
          ...doc.data(),
        });
      });

      return posts;
    } catch (error) {
      console.log("Error occured");
      return Promise.reject(error);
    }
  },
};