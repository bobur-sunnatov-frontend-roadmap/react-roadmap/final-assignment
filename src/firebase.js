import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// Change this config
const firebaseConfig = {
    apiKey: "AIzaSyCvDzwHEp2PxAdmTN6F_vF0RhJkVCiuJU4",
    authDomain: "final-b7126.firebaseapp.com",
    projectId: "final-b7126",
    storageBucket: "final-b7126.appspot.com",
    messagingSenderId: "1074143823650",
    appId: "1:1074143823650:web:0994d91d4101ee56f5477b",
    measurementId: "G-5LVESZ1DP9",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app);
const auth = getAuth(app);

export { db, auth };